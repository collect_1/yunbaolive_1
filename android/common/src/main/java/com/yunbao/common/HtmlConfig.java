package com.yunbao.common;

// +----------------------------------------------------------------------
// | Created by Yunbao
// +----------------------------------------------------------------------
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: https://gitee.com/yunbaokeji/
// +----------------------------------------------------------------------
// | Date: 2022-02-17
// +----------------------------------------------------------------------


public class HtmlConfig {
    //提现记录
    public static final String CASH_RECORD = CommonAppConfig.HOST + "/Appapi/cash/index";
    //支付宝充值回调地址
    public static final String ALI_PAY_COIN_URL = CommonAppConfig.HOST + "/Appapi/Pay/notify_ali";
    //支付宝购物下单支付回调地址
    //我的明细
    public static final String DETAIL = CommonAppConfig.HOST + "/Appapi/Detail/index";
    //充值协议
    public static final String CHARGE_PRIVCAY = CommonAppConfig.HOST + "/portal/page/index?id=6";
}
