package com.yunbao.live.socket;

import com.yunbao.common.bean.UserBean;
import com.yunbao.live.bean.LiveChatBean;
import com.yunbao.live.bean.LiveDanMuBean;
import com.yunbao.live.bean.LiveEnterRoomBean;
import com.yunbao.live.bean.LiveReceiveGiftBean;
import com.yunbao.live.bean.LiveUserGiftBean;

import java.util.List;
// +----------------------------------------------------------------------
// | Created by Yunbao
// +----------------------------------------------------------------------
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: https://gitee.com/yunbaokeji/
// +----------------------------------------------------------------------
// | Date: 2022-02-17
// +----------------------------------------------------------------------
/**
 * 直播间通用的socket逻辑
 */

public interface SocketMessageListener {

    /**
     * 直播间  连接成功socket后调用
     */
    void onConnect(boolean successConn);

    /**
     * 直播间  自己的socket断开
     */
    void onDisConnect();

    /**
     * 直播间  收到聊天消息
     */
    void onChat(LiveChatBean bean);


    /**
     * 直播间  收到用户进房间消息
     */
    void onEnterRoom(LiveEnterRoomBean bean);

    /**
     * 直播间  收到用户离开房间消息
     */
    void onLeaveRoom(UserBean bean);

    /**
     * 直播间  收到送礼物消息
     *
     * @param bean 礼物信息
     */
    void onSendGift(LiveReceiveGiftBean bean, LiveChatBean chatBean);


    /**
     * 直播间  收到弹幕消息
     */
    void onSendDanMu(LiveDanMuBean bean);

    /**
     * 直播间  观众收到直播结束消息
     */
    void onLiveEnd();

    /**
     * 直播间  主播登录失效
     */
    void onAnchorInvalid();

    /**
     * 直播间  超管关闭直播间
     */
    void onSuperCloseLive();


    /**
     * 直播间  主播切换计时收费或更改计时收费价格的时候执行
     */
    void onChangeTimeCharge(int typeVal);

    /**
     * 直播间  更新主播映票数
     */
    void onUpdateVotes(String uid, String addVotes, int first);

    /**
     * 直播间  添加僵尸粉
     */
    void addFakeFans(List<LiveUserGiftBean> list);


}
