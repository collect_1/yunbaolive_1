<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-02-17
// +—————————————————————————————————————————————————————————————————————
/**
 * 直播分类
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class LiveclassController extends AdminbaseController {
    public function index(){
			
    	$lists = Db::name("live_class")
            //->where()
            ->order("list_order asc, id desc")
            ->paginate(20);
            
        
        $page = $lists->render();

    	$this->assign('lists', $lists);

    	$this->assign("page", $page);
    	
    	return $this->fetch();
    }
		
    public function del(){
        
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('live_class')->where("id={$id}")->delete();
        if(!$rs){
            $this->error("删除失败！");
        }

        Db::name("live")->where(['liveclassid'=>$id])->update(['liveclassid'=>0]);
        Db::name("live_record")->where(['liveclassid'=>$id])->update(['liveclassid'=>0]);
        
       
                    
        $this->resetcache();
        $this->success("删除成功！");				
    }		
    //排序
    public function listOrder() { 
		
        $model = DB::name('live_class');
        parent::listOrders($model);
        
       
        
        $this->resetcache();
        $this->success("排序更新成功！");
    }	
    

    public function add(){        
        return $this->fetch();
    }	
    public function addPost(){
        if ($this->request->isPost()) {
            
            $data = $this->request->param();
            
			$name=$data['name'];

			if($name==""){
				$this->error("请填写名称");
			}
			$thumb=$data['thumb'];
			if($thumb==""){
				$this->error("请上传图标");
			}

            $data['thumb']=set_upload_path($thumb);

            $des=$data['des'];
            if($des==''){
                $this->error("请填写直播分类描述");
            }

            if(mb_strlen($des)>200){
                $this->error("直播分类描述在200字以内");
            }
            
			$id = DB::name('live_class')->insertGetId($data);
            if(!$id){
                $this->error("添加失败！");
            }
            
            
            
            $this->resetcache();
            $this->success("添加成功！");
            
		}
    }		
    public function edit(){
        
        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('live_class')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error("信息错误");
        }
        
        $this->assign('data', $data);
        return $this->fetch(); 			
    }
    
    public function editPost(){
        if ($this->request->isPost()) {
            
            $data      = $this->request->param();
            
			$name=$data['name'];

			if($name==""){
				$this->error("请填写名称");
			}
			$thumb=$data['thumb'];
			if($thumb==""){
				$this->error("请上传图标");
			}

			$des=$data['des'];
            if($des==''){
                $this->error("请填写直播分类描述");
            }

            if(mb_strlen($des)>200){
                $this->error("直播分类描述在200字以内");
            }

            $thumb_old=$data['thumb_old'];
            if($thumb_old!=$thumb){
                $data['thumb']=set_upload_path($thumb);
            }

            unset($data['thumb_old']);
            
			$id = DB::name('live_class')->update($data);
            if($id===false){
                $this->error("修改失败！");
            }
            
          
            
            $this->resetcache();
            $this->success("修改成功！");
		}	
    }
    
    public function resetcache(){
        $key='getLiveClass';
        $rules= DB::name('live_class')
                ->order('list_order asc,id desc')
                ->select();
        if($rules){
            setcaches($key,$rules);
        }else{
			delcache($key);
		}
        
        return 1;
    }
}
