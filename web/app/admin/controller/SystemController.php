<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-02-17
// +—————————————————————————————————————————————————————————————————————
/**
 * 直播间系统消息
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class SystemController extends AdminbaseController {
    public function index(){

		$config=getConfigPri();
			
		$this->assign('config', $config);
			
    	
    	return $this->fetch('edit');
    }
    public function send(){
		if ($this->request->isPost()) {
            
            $data      = $this->request->param();
            
			$content=$data['content'];

			if($content==''){
				$this->error("内容不能为空！");
			}
            
         
            
            $this->success("发送成功！");
		}
	}		
}
