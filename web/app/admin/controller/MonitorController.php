<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-02-17
// +—————————————————————————————————————————————————————————————————————
/**
 * 直播监控
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class MonitorController extends AdminbaseController {
    public function index(){

		$config=getConfigPri();
		$this->config=$config;
		$this->assign('config', $config);
        
        $lists = Db::name("live")
            ->where(['islive'=>1,'isvideo'=>0])
			->order("starttime desc")
			->paginate(6);
        
        $lists->each(function($v,$k){
			$v['userinfo']=getUserInfo($v['uid']);
            if($this->config['cdn_switch']==5){
                $auth_url=$v['pull'];
            }else{
                $auth_url=PrivateKeyA('http',$v['stream'].'.flv',0);
            }
            $v['url']=$auth_url;
            return $v; 
        });

        
        $page = $lists->render();

    	$this->assign('lists', $lists);

    	$this->assign("page", $page);
    	
    	return $this->fetch();

    }
    
	public function full()
	{
        $uid = $this->request->param('uid', 0, 'intval');
        
        $where['islive']=1;
        $where['uid']=$uid;
        
		$live=Db::name("live")->where($where)->find();
		$config=getConfigPri();
        
		if($live['title']=="")
		{
			$live['title']="直播监控后台";
		}
        

		$pull=urldecode(PrivateKeyA('http',$live['stream'].'.flv',0));
     
		$live['pull']=$pull;
		$this->assign('config', $config);
		$this->assign('live', $live);
        
		return $this->fetch();
	}
	public function stopRoom(){
        
		$uid = $this->request->param('uid', 0, 'intval');
        
        $where['islive']=1;
        $where['uid']=$uid;
        
		$liveinfo=Db::name("live")->field("uid,showid,starttime,title,province,city,stream,lng,lat,type,type_val,liveclassid")->where($where)->find();
        
		Db::name("live")->where(" uid='{$uid}'")->delete();
        
		if($liveinfo){
			$liveinfo['endtime']=time();
			$liveinfo['time']=date("Y-m-d",$liveinfo['showid']);
            
            $where2=[];
            $where2['touid']=$uid;
            $where2['showid']=$liveinfo['showid'];
            
			$votes=Db::name("user_coinrecord")
				->where($where2)
				->sum('totalcoin');
			$liveinfo['votes']=0;
			if($votes){
				$liveinfo['votes']=$votes;
			}
            
            $stream=$liveinfo['stream'];
			$nums=zSize('user_'.$stream);

			hDel("livelist",$uid);
			delcache($uid.'_zombie');
			delcache($uid.'_zombie_uid');
			delcache('attention_'.$uid);
			delcache('user_'.$stream);
			
			
			$liveinfo['nums']=$nums;
			
			Db::name("live_record")->insert($liveinfo);
            
 
		}

		$this->success("操作成功！");
	}				
}
