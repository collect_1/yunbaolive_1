<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-02-17
// +—————————————————————————————————————————————————————————————————————
namespace app\portal\controller;

use cmf\controller\AdminBaseController;

/**
 * Class AdminIndexController
 * @package app\portal\controller
 * @adminMenuRoot(
 *     'name'   =>'门户管理',
 *     'action' =>'default',
 *     'parent' =>'',
 *     'display'=> true,
 *     'order'  => 30,
 *     'icon'   =>'th',
 *     'remark' =>'门户管理'
 * )
 */
class AdminIndexController extends AdminBaseController
{


}
