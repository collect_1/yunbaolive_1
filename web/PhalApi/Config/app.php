<?php
/**
 * 请在下面放置任何您需要的应用配置
 */

return array(

    /**
     * 应用接口层的统一参数
     */
    'apiCommonRules' => array(
    ),
	
	
    'REDIS_HOST' => "127.0.0.1",
    'REDIS_AUTH' => "",
    'REDIS_PORT' => "",
    
    'sign_key' => '76576076c1f5f657b634e966c8836a06',
		
	'uptype'=>1,//上传方式：1表示 七牛，2表示 本地
	/**
     * 七牛相关配置
     */
    'Qiniu' =>  array(
        //ak
        'accessKey' => '',
        //sk
        'secretKey' => '',
        //存储空间
        'space_bucket' => '',
        //cdn加速域名 格式：http(s)://a.com，结尾不带/
        'space_host' => 'https://xxxx.com',  
        //区域上传域名(服务端)  参考文档：https://developer.qiniu.com/kodo/manual/1671/region-endpoint
        'uphost' => 'https://up.qiniup.com', 
        //七牛云存储区域 华东：z0，华北：z1，华南：z2，北美：na0，东南亚：as0，参考文档：https://developer.qiniu.com/kodo/manual/1671/
        'region'=>'z0',
    ),


		
	/**
     * 本地上传
     */
    'UCloudEngine' => 'local',

    /**
     * 本地存储相关配置（UCloudEngine为local时的配置）
     */
    'UCloud' => array(
        //对应的文件路径  站点域名
        'host' => 'http://x.com/upload' 
    ),
		
);
