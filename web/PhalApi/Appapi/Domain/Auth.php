<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-02-17
// +—————————————————————————————————————————————————————————————————————
class Domain_Auth {
	public function getAuth($uid) {
		$rs = array();

		$model = new Model_Auth();
		$rs = $model->getAuth($uid);

		return $rs;
	}
	
	
	public function setAuth($data) {
		$rs = array();

		$model = new Model_Auth();
		$rs = $model->setAuth($data);

		return $rs;
	}


	
}
